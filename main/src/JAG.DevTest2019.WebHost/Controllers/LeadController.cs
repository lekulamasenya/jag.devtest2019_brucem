﻿using JAG.DevTest2019.Host.Models;
using System;
using System.Net.Http;
using System.Web.Mvc;
using JAG.DevTest2019.Model;
using System.Web.Http;
using Newtonsoft.Json;

namespace JAG.DevTest2019.Host.Controllers
{
    public class LeadController : Controller
    {
        LeadsRepository leadRepo = new LeadsRepository();
        public ActionResult Index()
        {
            return View(new LeadViewModel());
        }

        [System.Web.Http.HttpPost]
        public ActionResult SubmitLead(LeadViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            //6. Call the WebAPI service here & pass results to UI
            var LeadId = new Random().Next();
            var lead = new Lead()
            {
                ClientIpAddress = "127.0.0.1",
                ContactNumber = model.ContactNumber,
                EmailAddress = model.EmailAddress,
                FirstName = model.FirstName,
                LeadId = LeadId,
                LeadParameters = null,
                ReferrerUrl = "www.jagmethod.com",
                Surname = model.Surname,
                TrackingCode = Guid.NewGuid().ToString("n").Substring(0, 20),
                UserAgent = "James Bond"
            };

            LeadService.Controllers.LeadController controller = new LeadService.Controllers.LeadController()
            {
                Request = new HttpRequestMessage(),
                Configuration = new HttpConfiguration()
            };
            var response = controller.Post(lead);
            var responseContent = response.Content.ReadAsStringAsync().Result;
            var leadResponse = JsonConvert.DeserializeObject<LeadResponse>(responseContent);

            LeadViewModel result = new LeadViewModel()
            {
                Results = new LeadResultViewModel()
                {
                    LeadId = leadResponse.LeadId,
                    IsSuccessful = leadResponse.IsSuccessful,
                    Message = string.Join("", leadResponse.Messages)
                }

            };

            return Json(result.Results);
        }


        [System.Web.Http.HttpGet]
        public ActionResult GetLeadStatus()
        {
            return Json(leadRepo.GetLeadStatus(), JsonRequestBehavior.AllowGet);
        }

        [System.Web.Http.HttpPost]
        public ActionResult RecordNewLeadId(int? id)
        {
            LeadService.Controllers.LeadController controller = new LeadService.Controllers.LeadController()
            {
                Request = new HttpRequestMessage(),
                Configuration = new HttpConfiguration()
            };

            controller.PollDB(id);
           
            return Json(new {LeadId = id });
        }

        [System.Web.Http.HttpGet]
        public ActionResult GetLeads()
        {
            return Json(leadRepo.GetData(), JsonRequestBehavior.AllowGet);
        }
    }
}