﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Configuration;


namespace JAG.DevTest2019.Host.Hubs
{
    public class LeadsHub : Hub
    {
        private static string conString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();


        [HubMethodName("showLeads")]
        public static void ShowLeads()
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<LeadsHub>();
            context.Clients.All.updateMessages();
            context.Clients.All.displayStatus();
        }


    }
}