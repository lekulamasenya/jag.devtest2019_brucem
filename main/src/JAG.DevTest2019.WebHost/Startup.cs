﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(JAG.DevTest2019.Host.Startup))]
namespace JAG.DevTest2019.Host
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}