﻿using JAG.DevTest2019.Host.Hubs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace JAG.DevTest2019.Host.Models
{
    public class LeadsRepository
    {
        public Int64 GetLeadStatus()
        {

            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(@"SELECT TOP 1
                                                           [LeadId]                                                          
                                                           FROM [dbo].[Lead]
                                                           ORDER BY ReceivedDateTime DESC", connection))
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    using (var reader = command.ExecuteReader())
                        return reader.Cast<IDataRecord>()
                            .Select(x => new LeadInfo()
                            {
                                LeadId = x.GetInt64(0),
                            }).FirstOrDefault().LeadId;
                }
            }
        }
        public IEnumerable<LeadInfo> GetData()
        {

            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(@"SELECT
                                                           [LeadId]
                                                          ,[TrackingCode]
                                                          ,[FirstName]
                                                          ,[LastName]
                                                          ,[ContactNumber]
                                                          ,[Email]
                                                          ,[ReceivedDateTime]
                                                          ,[IPAddress]
                                                          ,[UserAgent]
                                                          ,[ReferrerURL]
                                                          ,[IsDuplicate]
                                                          ,[IsCapped]
                                                          ,[IsSuccessful]
                                                           FROM [dbo].[Lead]", connection))
                {
                    command.Notification = null;

                    SqlDependency dependency = new SqlDependency(command);
                    dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    using (var reader = command.ExecuteReader())
                        return reader.Cast<IDataRecord>()
                            .Select(x => new LeadInfo()
                            {
                                LeadId = x.GetInt64(0),
                                TrackingCode = x.GetString(1),
                                FirstName = x.GetString(2),
                                LastName = x.GetString(3),
                                ContactNumber = x.GetString(4),
                                Email = x.GetString(5),
                                ReceivedDateTime = x.GetDateTime(6),
                                IPAddress = x.GetString(7),
                                UserAgent = x.GetString(8),
                                ReferrerURL = x.GetString(9),
                                IsDuplicate = x.GetBoolean(10),
                                IsCapped = x.GetBoolean(11),
                                IsSuccessful = x.GetBoolean(12),
                            }).ToList();
                }
            }
        }
        private void dependency_OnChange(object sender, SqlNotificationEventArgs e)
        {
            LeadsHub.ShowLeads();
        }
    }
}