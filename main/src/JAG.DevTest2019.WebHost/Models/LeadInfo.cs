﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JAG.DevTest2019.Host.Models
{
    public class LeadInfo
    {
        public Int64 LeadId { get; set; }
        public string TrackingCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactNumber { get; set; }
        public string Email { get; set; }
        public DateTime ReceivedDateTime { get; set; }
        public string IPAddress { get; set; }
        public string UserAgent { get; set; }
        public string ReferrerURL { get; set; }
        public bool IsDuplicate { get; set; }
        public bool IsCapped { get; set; }
        public bool IsSuccessful { get; set; }
    }
}