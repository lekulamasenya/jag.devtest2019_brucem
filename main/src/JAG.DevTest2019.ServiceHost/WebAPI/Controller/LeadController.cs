﻿using JAG.DevTest2019.Model;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace JAG.DevTest2019.LeadService.Controllers
{
    public class LeadController : ApiController
    {
        [HttpPost]
        [ResponseType (typeof(LeadResponse))]
        public HttpResponseMessage Post(Lead request)
        {
            LeadResponse response = new LeadResponse();

            if (CheckIfLeadExists(request))
            {
                response = new LeadResponse()
                {
                    IsDuplicate = true,
                    IsSuccessful = false,
                    IsCapped = false,
                    Messages = new[] { "duplicate on email / contact number" },
                };
            }
            else
            {
                response = new LeadResponse()
                {
                    LeadId = 10000000 + new Random().Next(),
                    IsCapped = false,
                    Messages = new[] { "Success from WebAPI" },
                    IsSuccessful = true,
                    IsDuplicate = false
                };
            }
          

            //7. Write the lead to the DB
            var insertStatement = @"INSERT INTO [dbo].[Lead]
                    ([LeadId],[TrackingCode],[FirstName],[LastName]
                    ,[ContactNumber],[Email],[ReceivedDateTime],[IPAddress]
                    ,[UserAgent],[ReferrerURL],[IsDuplicate],[IsCapped],[IsSuccessful])
                    VALUES
                    (@LeadId,@TrackingCode,@FirstName,@LastName,@ContactNumber,@Email,@ReceivedDateTime,
                    @IPAddress,@UserAgent,@ReferrerURL,@IsDuplicate,@IsCapped,@IsSuccessful)";

            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(insertStatement, connection))
                {
                        connection.Open();
                        command.Parameters.AddWithValue("@LeadId", request.LeadId);
                        command.Parameters.AddWithValue("@TrackingCode", request.TrackingCode);
                        command.Parameters.AddWithValue("@FirstName", request.FirstName);
                        command.Parameters.AddWithValue("@LastName", request.Surname);
                        command.Parameters.AddWithValue("@ContactNumber", request.ContactNumber);
                        command.Parameters.AddWithValue("@Email", request.EmailAddress);
                        command.Parameters.AddWithValue("@ReceivedDateTime", DateTime.Now);
                        command.Parameters.AddWithValue("@IPAddress", request.ClientIpAddress);
                        command.Parameters.AddWithValue("@UserAgent", request.UserAgent);
                        command.Parameters.AddWithValue("@ReferrerURL", request.ReferrerUrl);
                        command.Parameters.AddWithValue("@IsDuplicate", response.IsDuplicate);
                        command.Parameters.AddWithValue("@IsCapped", response.IsCapped);
                        command.Parameters.AddWithValue("@IsSuccessful", response.IsSuccessful);

                        int result = command.ExecuteNonQuery();
                        if (result < 0)
                        {
                            response.Messages = new[] { "An error occured while inserting data into database" };
                            Console.WriteLine($"An error occured while inserting data into database");
                        }

                        if (connection.State == System.Data.ConnectionState.Open)
                        {
                            connection.Close();
                        }
                }
            }

            Console.WriteLine($"Lead received {request.FirstName} {request.Surname}");

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }


        private bool CheckIfLeadExists(Lead request)
        {
            var selectStatement = @"SELECT COUNT(1) FROM [dbo].[Lead]
                                    WHERE ContactNumber like '@ContactNumber'
                                    OR
                                    Email like '@Email'";

            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(selectStatement, connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@ContactNumber", request.ContactNumber);
                    command.Parameters.AddWithValue("@Email", request.EmailAddress);

                    int result = (int) command.ExecuteScalar();
                    if (result > 0)
                    {
                        return true;
                    }

                    if (connection.State == System.Data.ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }
            }

            return false;
        }


        [HttpPost]
        public void PollDB(int? id)
        {
            Console.WriteLine($"New lead record Id :  { id } ");
        }
    }
}
