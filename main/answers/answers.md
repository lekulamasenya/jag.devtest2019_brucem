#JAG Method software developer assessment
## Answers

### 1. SEO (5min)

1) Add keywords in page title
2) Add keywords in meta description
3) Add keywords in meta keywords
4) Modify the actionlink to point to SEO friendly URL (E.g. http://localhost:49391/Lead to http://localhost:49391/Get-a-quote-online)


### 2. Responsive (15m)

1) Fixed header (.header class was overriding base class)
2) Body style
3) Add lead Form responsiveness
4) ...

### 3 - 6 Answers in code

### 7. ADO.Net (40m)
Add any SQL schema changes here

### 8. Poll DB (15m)
Add any SQL schema changes here

### 9. SignalR (40m)
Add any SQL schema changes here

### 10. Data Analysis (30m)

1) Total Profit
**Answer**
-67870.59685063502800

**SQL**
`Select....`
SELECT SUM([Earnings]) - Sum([Cost]) as Profit
 FROM [JAG2019].[dbo].[LeadDetail]

2) Total Profit (Earnings less VAT)
**Answer**
-147071.05245236561380

**SQL**
`Select....`
 SELECT (SUM([Earnings] * 15) / 100) - Sum([Cost]) as Profit
 FROM [JAG2019].[dbo].[LeadDetail]

3) Profitable campaigns
**Answer**
Campaign 161 and Campaign 92 (Top 2)

**SQL**
`Select....`
SELECT Campaign.CampaignName,
	   MAX(LeadDetail.Earnings - LeadDetail.Cost) as Profit
  FROM [JAG2019].[dbo].[LeadDetail]
  INNER JOIN [JAG2019].[dbo].[Campaign] ON Campaign.CampaignId = LeadDetail.CampaignId
  GROUP BY
  Campaign.CampaignName
  ORDER BY Profit DESC

4) Average conversion rate
**Answer**
0.0112767584097859 % = 59 / 5232;
SELECT FORMAT((59/5232),'P') as [Percentage]

**SQL**
`Select....`
SELECT Campaign.CampaignName,
  AVG(LeadDetail.Earnings - LeadDetail.Cost) as ConversionRate
  FROM [JAG2019].[dbo].[LeadDetail]
  INNER JOIN [JAG2019].[dbo].[Campaign] ON Campaign.CampaignId = LeadDetail.CampaignId
  WHERE LeadDetail.IsSold = 1 AND LeadDetail.IsAccepted = 1
  GROUP BY
  Campaign.CampaignName


5) Pick 2 clients based on Profit & Conversion rate & Why?
**Answer**
Client 215 and Client 168
Quality is an important as quantity - Lead generation is critical to the sales process.
This means client 215 and 168 does produce quality leads with a good chance of closing and that means
they have a potential to become potential customers.

**SQL**
`Select....`
  SELECT 
  Client.ClientName,
  AVG(LeadDetail.Earnings - LeadDetail.Cost) as ConversionRate
  FROM [JAG2019].[dbo].[LeadDetail]
  INNER JOIN [JAG2019].[dbo].[Campaign] ON Campaign.CampaignId = LeadDetail.CampaignId
  INNER JOIN [JAG2019].[dbo].[Client] ON Client.ClientId = LeadDetail.ClientId
  WHERE LeadDetail.IsSold = 1 AND LeadDetail.IsAccepted = 1
    GROUP BY
 Client.ClientName
  ORDER BY ConversionRate DESC

  SELECT 
  Client.ClientName,
  MAX(LeadDetail.Earnings - LeadDetail.Cost) as Profit
  FROM [JAG2019].[dbo].[LeadDetail]
  INNER JOIN [JAG2019].[dbo].[Campaign] ON Campaign.CampaignId = LeadDetail.CampaignId
  INNER JOIN [JAG2019].[dbo].[Client] ON Client.ClientId = LeadDetail.ClientId
  WHERE LeadDetail.IsSold = 1 AND LeadDetail.IsAccepted = 1
    GROUP BY
  Client.ClientName
  ORDER BY Profit DESC